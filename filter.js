const filter = (elements,cb) => {
    if(!elements) return []

    let result = []

    for(i =0; i <elements.length;i++){
        if(cb(elements[i],i)){
            result.push(elements[i])
        }
    }

    return result
}

module.exports = filter