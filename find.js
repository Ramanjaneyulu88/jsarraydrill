let items = [1, 2, 3, 4, 5, 5];

function find(elements, cb) {
if(!elements) return 

  for (i = 0; i < elements.length; i++) {
    if (cb(elements[i], i)) {
      return elements[i];
    }
  }
}

// callback

module.exports = find