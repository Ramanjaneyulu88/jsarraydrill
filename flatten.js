const flatten = (nestedArray,depth = 1, newArr) =>{
    for (let item of nestedArray){
        if(Array.isArray(item) && depth > 0){
            flatten(item,depth-1,newArr)
        }else{
            newArr.push(item)
        }
    }
    return newArr
}

module.exports = flatten