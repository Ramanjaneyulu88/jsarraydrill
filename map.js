

const map = (elements,cb) => {

    if(elements === undefined) return []
    let newArray = []

    for(let i = 0; i <elements.length; i++){
        newArray.push(cb(elements[i],i,elements))
    }

   return newArray
}


module.exports = map
